Knuth method: lots of details, and input model.

Differences Knuth method and theory of algorithms:

by analysing worst case takes the input model out of the picture.

Q: I don't think I understand this.

Drawback: not suitable for scientific studies. (can't do the
experiments as you can with precise predictions).

Analytic combinatorics solves these problems: provides calculus for
input models, and provides universal laws that encompass the detail of
the analysis.

Philippe Flajolet; Automatic analysis of algorithms (can't get there
b/c halting problem, but in practise can get very close).

Analytic combinatorics is a calculus for the quantitive study of large
combinatorial structures.

step 1: specify the class through combinatorial construction.

step 2: introduce generating functions and use symbolic transfer theorems.

step 3: extract coefficients using analytic transfer theorems.

Can use related methods for generating random structures.
